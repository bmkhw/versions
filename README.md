# Самые новые исправления
| Прибор | 64-bit | 32-bit | Известные проблемы |
| --- | --- | --- | --- |
| Общее | [bmkhw64-2015.dll.2054](../../raw/master/bmkhw64-2015.dll.2054) | [bmkhw32-2015.dll.2054](../../raw/master/bmkhw32-2015.dll.2054) | #3122 (CRITICAL since 2046), details available below |
| [ExVacuum](https://gitlab.com/bmkhw/versions/-/blob/master/ExVacuum/README.md) | [ExVacuum2015.dll.2019](../../raw/master/ExVacuum/ExVacuum2015.dll.2019) [bmkhw64.dll.2020](../../raw/master/ExVacuum/bmkhw64.dll.2020) | | |
| [Везувий-3](https://gitlab.com/bmkhw/versions/-/blob/master/Vezuvy/README.md) |
| [Абсорбция](https://gitlab.com/bmkhw/versions/-/blob/master/Absorb/README.md) |
| [СВЧ](https://gitlab.com/bmkhw/versions/-/blob/master/exsvch/README.md) |
| [Колибри-USB](https://gitlab.com/bmkhw/versions/-/blob/master/FTDI/README.md) | [bmkhw64-2015.dll.2023-FTDI](../../raw/master/FTDI/bmkhw64-2015.dll.2023-FTDI) | [bmkhw32-2015.dll.2023-FTDI](../../raw/master/FTDI/bmkhw32-2015.dll.2023-FTDI) | [#3000](https://dev.vmk.ru/redmine/issues/3000) |
| [1Gb](https://gitlab.com/bmkhw/versions/-/tree/master/1Gb/README.md) | [bmkhw64.dll.2021](../../raw/master/1Gb/bmkhw64.dll.2021) | | |
| [2xExp](https://gitlab.com/bmkhw/versions/-/tree/master/2xExp/README.md) | [bmkhw64-2015.dll.2051-2xExp](../../../raw/master/2xExp/bmkhw64-2015.dll.2051-2xExp) | [bmkhw32-2015.dll.2051-2xExp](../../raw/master/2xExp/bmkhw32-2015.dll.2051-2xExp) | |

# versions history
| 64-bit | 32-bit | Исправления |
| --- | --- | --- |
| [bmkhw64-2015.dll.2099](../../raw/master/bmkhw64-2015.dll.2099) | [bmkhw32-2015.dll.2099](../../raw/master/bmkhw32-2015.dll.2099) | ExtControl: Переименованы БЛПП-02Б/БЛПП-02В в БЛПП-2000/БЛПП-4000, добавлены типы кристаллов БЛПП-4100/ИК-256 (0xB, 0xC) |
| [bmkhw64-2015.dll.2080](../../raw/master/bmkhw64-2015.dll.2080) |  | Error code when external device is unselected |
| [bmkhw64-2015.dll.2079](../../raw/master/bmkhw64-2015.dll.2079) |  | Support more than 2 external devices |
| [bmkhw64-2015.dll.2078](../../raw/master/bmkhw64-2015.dll.2078) |  | New Device/Interface descriptor support |
| [bmkhw64-2015.dll.2077.5000buffer](../../raw/master/bmkhw64-2015.dll.2077.5000buffer) |  | 2077 with extended buffer to 5k |
| [bmkhw64-2015.dll.2071](../../raw/master/bmkhw64-2015.dll.2071) |  | 2071 CloseHandle in progressor |
| [bmkhw64-2015.dll.2068](../../raw/master/bmkhw64-2015.dll.2068) | [bmkhw32-2015.dll.2068](../../raw/master/bmkhw32-2015.dll.2069) | 2068 Extended progressor stages |
| [bmkhw64-2015.dll.2054](../../raw/master/bmkhw64-2015.dll.2054) | [bmkhw32-2015.dll.2054](../../raw/master/bmkhw32-2015.dll.2054) | 2054 [1a22] Buffer-overrun in CDialogMirror (#3150), additionally control line_order via MAX_LINE_ORDER (was 62, now 64 as defines Masiudp.h). TODO: CDialogMirror supports only 24 |
| [bmkhw64-2015.dll.2053](../../raw/master/bmkhw64-2015.dll.2053) | [bmkhw32-2015.dll.2053](../../raw/master/bmkhw32-2015.dll.2053) | 200% #3136 fix, Reverted '[0c28] to handle correctly handled = true' (#3134), follow-up: #3147, #3132, #2924, #2553 |
| [bmkhw64-2015.dll.2052](../../raw/master/bmkhw64-2015.dll.2052) | [bmkhw32-2015.dll.2052](../../raw/master/bmkhw32-2015.dll.2052) | EX_GET_DOUBLE_EXPOSITION_PARAMS fix, Propagate LP_MIRROR_LINES to AsyncDevice, Remove leak at CDescriptorDeviceImpl::getDeviceInterface |
| [bmkhw64-2015.dll.2051](../../raw/master/bmkhw64-2015.dll.2051) | [bmkhw32-2015.dll.2051](../../raw/master/bmkhw32-2015.dll.2051) | #3122 (CRITICAL since 2046) Avoid progressor thread creation from multiple devices (#3122), Propagate host/device ip before opening the driver (CDeviceMulti) |
| [bmkhw64-2015.dll.2048](../../raw/master/bmkhw64-2015.dll.2048) | [bmkhw32-2015.dll.2048](../../raw/master/bmkhw32-2015.dll.2048) | #3101, #2886: Hard sync progressor exit, do not forget progressor color if it is restarted |
| [bmkhw64-2015.dll.2046](../../raw/master/bmkhw64-2015.dll.2046) | [bmkhw32-2015.dll.2046](../../raw/master/bmkhw32-2015.dll.2046) | #3102 fix progressor width initialization, avoid potential deadlock in progressor|
| [bmkhw64-2015.dll.2045](../../raw/master/bmkhw64-2015.dll.2045) | [bmkhw32-2015.dll.2045](../../raw/master/bmkhw32-2015.dll.2045) | #3098 (CRITICAL) Fix typo in notify_fixCarry call (fun_code and lines_rate parameters swap)|
| [bmkhw64-2015.dll.2044](../../raw/master/bmkhw64-2015.dll.2044) | [bmkhw32-2015.dll.2044](../../raw/master/bmkhw32-2015.dll.2044) | CProgressCtrlExtended: Do not re-create data structures, Extra synchronization in progressor thread |
| [bmkhw64-2015.dll.2042](../../raw/master/bmkhw64-2015.dll.2042) | [bmkhw32-2015.dll.2042](../../raw/master/bmkhw32-2015.dll.2042) | EXPERIMENTAL_LMA10 re-enabled, Allow merging IS_ASKLINE into single read, pre-2041 behaviour was handle_ask_line==false |
| [bmkhw64-2015.dll.2041](../../raw/master/bmkhw64-2015.dll.2041) | [bmkhw32-2015.dll.2041](../../raw/master/bmkhw32-2015.dll.2041) | EXPERIMENTAL_LMA10 [0225] для LMA 10 запрос одним кадром (Polivak) |
| [bmkhw64-2015.dll.2040](../../raw/master/bmkhw64-2015.dll.2040) | [bmkhw32-2015.dll.2040](../../raw/master/bmkhw32-2015.dll.2040) | Progressor fixes (polivak) |
| [bmkhw64-2015.dll.2038](../../raw/master/bmkhw64-2015.dll.2038) | [bmkhw32-2015.dll.2038](../../raw/master/bmkhw32-2015.dll.2038) | Increase ICommonEvents::MAX_EVENTS from 1024 to 1020*10 for 9M read 0.4ms (Oleg) |
| [bmkhw64-2015.dll.2036](../../raw/master/bmkhw64-2015.dll.2036) | [bmkhw32-2015.dll.2036](../../raw/master/bmkhw32-2015.dll.2036) | #2886 [2002][Вакуумный] Прогрессор "забыл" о продувке - рисует обжиг и экспозицию. |
| [bmkhw64-2015.dll.2035](../../raw/master/bmkhw64-2015.dll.2035) | [bmkhw32-2015.dll.2035](../../raw/master/bmkhw32-2015.dll.2035) | Используется Олегом, #3083 IS_ASK_IMAGE_SYNC #3076 TWO_MATRICE OVERFLOW #2908 LP_CUT_DATA #3075 [2xExp] Переворот всего меняет местами экспозиции, IS_DOUBLE_EXPOSITION_MODE, EX_GET_DOUBLE_EXPOSITION_PARAMS, EX_SET_DOUBLE_EXPOSITION_PARAMS, #3084, #2908, #3085, LONGDEST_IS_BAD |
| [bmkhw64-2015.dll.2024](../../raw/master/bmkhw64-2015.dll.2024) | [bmkhw32-2015.dll.2025](../../raw/master/bmkhw32-2015.dll.2025) | #2908 Исключены падения при установленной настройке "Отсечение", а также при переключении 1x2048 -> 14x2048 при снятом темновом токе. FTDI: Вычитание темнового в 32-бит |
| [bmkhw64.dll.1974](../../raw/master/bmkhw64.dll.1974) |  |  |
| [bmkhw64.dll.1973](../../raw/master/bmkhw64.dll.1973) |  | Работают датчики на Гранд-СВЧ (Сергей Б.) |

# Установленные версии
| Прибор | Дата отправки | Ответственный | Заказчик | 32/64 | Атом | bmkhware | Внешний модуль |
| --- | --- | --- | --- | --- | --- | --- | --- |
| Колибри-USB | Май 2021 | Денис Селюнин | Институт? | 64? | Апрель 2021? | [bmkhw64-2015.dll.2016-FTDI](../../raw/master/FTDI/bmkhw64-2015.dll.2016-FTDI) | n/a |

# Версии в разработке (с известными проблемами)
| Общее | [bmkhw64-2015.dll.2034](../../raw/master/bmkhw64-2015.dll.2034) | [bmkhw32-2015.dll.2034](../../raw/master/bmkhw32-2015.dll.2034) | #3084 В связи с доработками 2xExp, #2908 |
