# ExAbsorb files

# 64-bit Visual Studio 2015
| file | redmine | description |
| ------ | ------ | ------ |
| [bmkhw64-2015.dll.2054](../../../raw/master/bmkhw64-2015.dll.2054) |  | Рекомендуемая версия bmkhware.dll (самая свежая, 2021-10-22) |
| [bmkhw64-2015.dll.2019](../../../raw/master/ExAbsorb/bmkhw64-2015.dll.2019) | [#2924](https://dev.vmk.ru/redmine/issues/2924) | Версия из 2021-03-28-ExAbsorb.2019.zip (#2924, fix message handling) |
| [ExAbsorb2015.dll.2019](../../../raw/master/ExAbsorb/ExAbsorb2015.dll.2019) | [#2924](https://dev.vmk.ru/redmine/issues/2924) | Версия из 2021-03-28-ExAbsorb.2019.zip (#2924, fix message handling) |

# 32-bit Visual Studio 2015
| file | redmine | description |
| ------ | ------ | ------ |
| [bmkhw32-2015.dll.2054](../../../raw/master/bmkhw32-2015.dll.2054) |  | Рекомендуемая версия bmkhware.dll (самая свежая, 2021-10-22) |
| [bmkhw32-2015.dll.2019](../../../raw/master/ExAbsorb/bmkhw32-2015.dll.2019) | [#2924](https://dev.vmk.ru/redmine/issues/2924) | Версия из 2021-03-28-ExAbsorb.2019.zip (#2924, fix message handling) |
| [ExAbsorb2015-32.dll.2019](../../../raw/master/ExAbsorb/ExAbsorb2015-32.dll.2019) | [#2924](https://dev.vmk.ru/redmine/issues/2924) | Версия из 2021-03-28-ExAbsorb.2019.zip (#2924, fix message handling) |

<pre>
// 2019 [1328] [ExAbsorb] #2924 Fix message handling

Instructions:
32-bit:
- Remove bmkhware.dll in Atom32 folder
- Remove exabsorb*.dll.* in Atom32\HDll folder (we have to remove all versions there)
- Copy bmkhw32-2015.dll.<version> to bmkhware.dll in Atom32 folder
- Copy ExAbsorb2015-32.dll.<version> to ExAbsorb.dll in Atom32\HDll folder

64-bit:
- Remove bmkhw64.dll in Atom64 folder
- Remove exabsorb*.dll.* in Atom64\HDll folder (we have to remove all versions there)
- Copy bmkhw64-2015.dll.<version> to bmkhw64.dll in Atom64 folder
- Copy ExAbsorb2015.dll.<version> to ExAbsorb.dll in Atom64\HDll folder
<pre>