// 2019 [1328] [ExAbsorb] #2924 Fix message handling

Instructions:
32-bit:
- Remove bmkhware.dll in Atom32 folder
- Remove exabsorb*.dll.* in Atom32\HDll folder (we have to remove all versions there)
- Copy bmkhw32-2015.dll.<version> to bmkhware.dll in Atom32 folder
- Copy ExAbsorb2015-32.dll.<version> to ExAbsorb.dll in Atom32\HDll folder

64-bit:
- Remove bmkhw64.dll in Atom64 folder
- Remove exabsorb*.dll.* in Atom64\HDll folder (we have to remove all versions there)
- Copy bmkhw64-2015.dll.<version> to bmkhw64.dll in Atom64 folder
- Copy ExAbsorb2015.dll.<version> to ExAbsorb.dll in Atom64\HDll folder
