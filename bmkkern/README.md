# 1Gb Maes
# 64-bit

| file | redmine | description |
| ------ | ------ | ------ |
| [bmkkern64.dll.2049](../../../raw/master/bmkkern/bmkkern64.dll.2049) |  | 64-bit build |

# 32-bit
| file | redmine | description |
| ------ | ------ | ------ |
| [bmkkern32.dll.2038](../../../raw/master/bmkkern/bmkkern32.dll.2038) |  | VS2015 build |
| [bmkkern32.dll.2048](../../../raw/master/bmkkern/bmkkern32.dll.2048) |  | CDialogProgress unused |
| [bmkhw32-2015.dll.2038](../../../raw/master/bmkkern/bmkhw32-2015.dll.2038) |  | Latest bmkhare.dll (VS2015) |
| [bmkhw32-2015.dll.2039](../../../raw/master/bmkkern/bmkhw32-2015.dll.2039) |  | 2039 [1a02] Sync up with VirtualMAES.exe polivak, Atom 13.09.2021 |
| [bmkhw32-2015.dll.2040](../../../raw/master/bmkkern/bmkhw32-2015.dll.2040) |  | Progressor fixes (polivak) |
| [bmkhw32-2015.dll.2047](../../../raw/master/bmkkern/bmkhw32-2015.dll.2047) |  | 2047 [1a06] #3104: WS_EX_TOPMOST style for IDD_DLG_PROGRESSOR |
| [bmkhw32-2015.dll.2048](../../../raw/master/bmkkern/bmkhw32-2015.dll.2048) |  | #3101, #2886: Hard sync progressor exit, do not forget progressor color if it is restarted |

# Установленные версии
| Прибор | Дата отправки | Ответственный | Заказчик | 32/64 | Атом | bmkhware | Внешний модуль |
| --- | --- | --- | --- | --- | --- | --- | --- |
| Поливак | 09/07/2006 | Виктор | ABC | 32 | ? | [bmkhware.dll.0545 ?](../../raw/master/bmkkern/bmkhware.dll.0545) | [bmkkern.dll.0545](../../raw/master/bmkkern/bmkkern.dll.0545) |

Виктор (2021-10-02): 677 заработало с Атом 13.09.2021 и 0545? bmkkern, в 777 и последующих проблема с прогрессором
