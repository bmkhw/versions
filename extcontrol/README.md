# ExtControl
# 64-bit

| file | redmine | description |
| ------ | ------ | ------ |
| [ExtControl64.exe.2025-02-08](../../../raw/master/extcontrol/ExtControl64.exe.2025-02-08) |  | VS2015 updated build |
| [bmkhw64-2015.dll.2101](../../raw/master/extcontrol/bmkhw64-2015.dll.2101) | ExtControl: extcontrol: �� ������������� �� ������������ ��� ����-2000 |
| [bmkhw64-2015.dll.2100](../../raw/master/extcontrol/bmkhw64-2015.dll.2100) | ExtControl: �� ��������� ���� ���� ��� ����-4000, �������� ������ ��� ������� ������������ |
| [bmkhw64-2015.dll.2099](../../raw/master/extcontrol/bmkhw64-2015.dll.2099) | ExtControl: ������������� ����-02�/����-02� � ����-2000/����-4000, ��������� ���� ���������� ����-4100/��-256 (0xB, 0xC) |
| [ExtControl64.exe.2021-10-08](../../../raw/master/extcontrol/ExtControl64.exe.2021-10-08) |  | VS2015 build |

# 32-bit
| file | redmine | description |
| ------ | ------ | ------ |
| [extcontrol.exe.2005-10-27](../../../raw/master/extcontrol/extcontrol.exe.2005-10-27) |  | extcontrol from Atom x32 3.3 (2021.09.13).exe (works with bmkhware.dll.0835) |
| [bmkhw32-2015.dll.2101](../../raw/master/extcontrol/bmkhw32-2015.dll.2101) | ExtControl: extcontrol: �� ������������� �� ������������ ��� ����-2000 |
| [bmkhw32-2015.dll.2100](../../raw/master/extcontrol/bmkhw32-2015.dll.2100) | ExtControl: �� ��������� ���� ���� ��� ����-4000, �������� ������ ��� ������� ������������ |
| [bmkhw32-2015.dll.2099](../../raw/master/extcontrol/bmkhw32-2015.dll.2099) | ExtControl: ������������� ����-02�/����-02� � ����-2000/����-4000, ��������� ���� ���������� ����-4100/��-256 (0xB, 0xC) |
| [bmkhware.dll.0835](../../../raw/master/extcontrol/bmkhware.dll.0835) |  | extcontrol from Atom x32 3.3 (2021.09.13).exe |
| [ExtControl32.exe.2025-02-08](../../../raw/master/extcontrol/ExtControl32.exe.2025-02-08) |  | VS2015 updated build |
| [ExtControl32.exe.2021-10-20](../../../raw/master/extcontrol/ExtControl32.exe.2021-10-20) |  | ������� �� bmkhware.dll, � �� �� bmkhw32.dll ��� � 2021-10-08, ��� ������� ���������� ��� ������������� � ���������� ���� |
| [ExtControl32.exe.2021-10-08](../../../raw/master/extcontrol/ExtControl32.exe.2021-10-08) |  | VS2015 build |
