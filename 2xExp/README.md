# 1Gb Maes
# 64-bit

| file | redmine | description |
| ------ | ------ | ------ |
| [bmkhw64-2015.dll.2061-2X](../../../raw/master/2exp/bmkhw64-2015.dll.2061-2X) |  | Swap also M/N, fix SetLineLength (x2), OK in settings dialog causes UpdateTime if mode is changed |
| [bmkhw64-2015.dll.2060-2X](../../../raw/master/2exp/bmkhw64-2015.dll.2060-2X) |  | Replace tau1/tau2, fixes |
| [bmkhw64-2015.dll.2059-2X](../../../raw/master/2exp/bmkhw64-2015.dll.2059-2X) |  | 03/02 Review fixes #3184 (detect 2X support, device_descriptor update), CURRENT_EXPOSITION update for 2X mode |
| [bmkhw64-2015.dll.2058-2X](../../../raw/master/2exp/bmkhw64-2015.dll.2058-2X) |  | M/N: #3181 (PRM4_SetChipsPixelNumber), #3184 (detect 2X support), UDM_MAES_COUNTER temporary fix |
| [bmkhw64-2015.dll.2057-2X](../../../raw/master/2exp/bmkhw64-2015.dll.2057-2X) |  | Ожидается работа простой схемы чтения с параметрами M/N в одиночном устройстве |
| [bmkhw64-2015.dll.2055-2X](../../../raw/master/2exp/bmkhw64-2015.dll.2055-2X) |  | Возможность задания M/N в одиночном устройстве (in progress) |
| [bmkhw64-2015.dll.2054-2xExp](../../../raw/master/2exp/bmkhw64-2015.dll.2054-2xExp) |  | 2054 [1a22] Buffer-overrun in CDialogMirror (#3150), additionally control line_order via MAX_LINE_ORDER (was 62, now 64 as defines Masiudp.h). TODO: CDialogMirror supports only 24 |
| [bmkhw64-2015.dll.2053-2xExp](../../../raw/master/2exp/bmkhw64-2015.dll.2053-2xExp) |  | Reverted '[0c28] to handle correctly handled = true' (#3134), follow-up: #3147, #3132, #2924, #2553 |
| [bmkhw64-2015.dll.2051-2xExp](../../../raw/master/2exp/bmkhw64-2015.dll.2051-2xExp) |  | #3122 (CRITICAL since 2046) Avoid progressor thread creation from multiple devices (#3122), Propagate host/device ip before opening the driver (CDeviceMulti) |
| [bmkhw64-2015.dll.2050-2xExp](../../../raw/master/2exp/bmkhw64-2015.dll.2050-2xExp) |  | #3121 PRM4_WriteRelePrgMem |
| [bmkhw64-2015.dll.2049-2xExp](../../../raw/master/2exp/bmkhw64-2015.dll.2049-2xExp) |  | #3105 Workaround dark corr for CDeviceMulti |
| [bmkhw64-2015.dll.2048-2xExp](../../../raw/master/2exp/bmkhw64-2015.dll.2048-2xExp) |  | #3105 Workaround dark corr for CDeviceMulti |
| [bmkhw64-2015.dll.2046-2xExp](../../../raw/master/2exp/bmkhw64-2015.dll.2046-2xExp) |  | Ряд исправлений в прогрессоре, LMA10 |
| [bmkhw64-2015.dll.2036-2xExp](../../../raw/master/2exp/bmkhw64-2015.dll.2036-2xExp) |  | #2886 [2002][Вакуумный] Прогрессор "забыл" о продувке - рисует обжиг и экспозицию. |
| [bmkhw64-2015.dll.2035-2xExp](../../../raw/master/2exp/bmkhw64-2015.dll.2035-2xExp) |  | #3084, #2908, #3083, #3085 |
| [bmkhw64-2015.dll.2032-2xExp](../../../raw/master/2exp/bmkhw64-2015.dll.2032-2xExp) |  | #3083 IS_ASK_IMAGE_SYNC #3076 TWO_MATRICE OVERFLOW #2908 LP_CUT_DATA|
| [bmkhw64-2015.dll.2030-2xExp](../../../raw/master/2exp/bmkhw64-2015.dll.2030-2xExp) |  | #3075 [2xExp] Переворот всего меняет местами экспозиции |
| [bmkhw64.dll.1959-2xExp](../../../raw/master/2exp/bmkhw64.dll.1959-2xExp) |  | IS_DOUBLE_EXPOSITION_MODE, EX_GET_DOUBLE_EXPOSITION_PARAMS, EX_SET_DOUBLE_EXPOSITION_PARAMS (бранч 2019 года), -half_line for IPS_OFFSET_MULTI_RIGHT, fixCarry->dest_size_total |
| [bmkhw64-2015.dll.2029-2xExp](../../../raw/master/2exp/bmkhw64-2015.dll.2029-2xExp) |  | Составное |
| [bmkhw64.dll.1957-2xExp](../../../raw/master/2exp/bmkhw64.dll.1957-2xExp) |  | Перестановка в соответствии с двухстрочной в составном устройстве (бранч 2019 года) |
| [bmkhw64.dll.1956-2xExp](../../../raw/master/2exp/bmkhw64.dll.1956-2xExp) |  | Распознавание Везувий-СВЧ, переворот (бранч 2019 года) |
| [bmkhw64-2015.dll.2027-2xExp](../../../raw/master/2exp/bmkhw64-2015.dll.2027-2xExp) |  | Составное |
| [bmkhw64-2015.dll.2026-2xExp](../../../raw/master/2exp/bmkhw64-2015.dll.2026-2xExp) |  | Распознавание Везувий-СВЧ, переворот |
| [bmkhw64.dll.1954-2xExp](../../../raw/master/2exp/bmkhw64.dll.1954-2xExp) |  | Используется Сергеем Б. |

# 32-bit
| file | redmine | description |
| ------ | ------ | ------ |
| [bmkhw32-2015.dll.2055-2X](../../../raw/master/2exp/bmkhw64-2015.dll.2055-2X) |  | Возможность задания M/N в одиночном устройстве (in progress) |
| [bmkhw32-2015.dll.2054-2xExp](../../../raw/master/2exp/bmkhw32-2015.dll.2054-2xExp) |  | 2054 [1a22] Buffer-overrun in CDialogMirror (#3150), additionally control line_order via MAX_LINE_ORDER (was 62, now 64 as defines Masiudp.h). TODO: CDialogMirror supports only 24 |
| [bmkhw32-2015.dll.2053-2xExp](../../../raw/master/2exp/bmkhw32-2015.dll.2053-2xExp) |  | Reverted '[0c28] to handle correctly handled = true' (#3134), follow-up: #3147, #3132, #2924, #2553 |
| [bmkhw32-2015.dll.2051-2xExp](../../../raw/master/2exp/bmkhw32-2015.dll.2051-2xExp) |  | #3122 (CRITICAL since 2046) Avoid progressor thread creation from multiple devices (#3122), Propagate host/device ip before opening the driver (CDeviceMulti) |
| [bmkhw32-2015.dll.2049-2xExp](../../../raw/master/2exp/bmkhw32-2015.dll.2049-2xExp) |  | #3105 Workaround dark corr for CDeviceMulti |
| [bmkhw32-2015.dll.2048-2xExp](../../../raw/master/2exp/bmkhw32-2015.dll.2048-2xExp) |  | #3105 Workaround dark corr for CDeviceMulti |
| [bmkhw32-2015.dll.2036-2xExp](../../../raw/master/2exp/bmkhw32-2015.dll.2036-2xExp) |  | #2886 [2002][Вакуумный] Прогрессор "забыл" о продувке - рисует обжиг и экспозицию. |
| [bmkhw32-2015.dll.2035-2xExp](../../../raw/master/2exp/bmkhw32-2015.dll.2035-2xExp) |  | #3084, #2908, #3083, #3085 |
| [bmkhw32-2015.dll.2032-2xExp](../../../raw/master/2exp/bmkhw32-2015.dll.2032-2xExp) |  | #3083 IS_ASK_IMAGE_SYNC #3076 TWO_MATRICE OVERFLOW #2908 LP_CUT_DATA |
| [bmkhw32-2015.dll.2029-2xExp](../../../raw/master/2exp/bmkhw32-2015.dll.2029-2xExp) |  | Составное |
| [bmkhw32-2015.dll.2027-2xExp](../../../raw/master/2exp/bmkhw32-2015.dll.2027-2xExp) |  | Составное |
| [bmkhw32-2015.dll.2026-2xExp](../../../raw/master/2exp/bmkhw32-2015.dll.2026-2xExp) |  | Распознавание Везувий-СВЧ, переворот |

Сергей Бабин (2021-10-15): используется 1973 в одноэкспозиционном режиме