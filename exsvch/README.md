# ExVaccum files (Гранд-Эксперт)
# 64-bit

| file | redmine | description |
| ------ | ------ | ------ |
| [exsvch2015-64.dll.2027](../../../raw/master/exsvch/exsvch2015-64.dll.2027) |  | Сборка версии 1964 в Visual Studio 2015 |

# 32-bit
| file | redmine | description |
| ------ | ------ | ------ |
| [exsvch2015-32.dll.2027](../../../raw/master/exsvch/exsvch2015-32.dll.2027) |  | Сборка версии 1964 в Visual Studio 2015 |
