# ExVaccum files (Гранд-Эксперт)
# 64-bit

| file | redmine | description |
| ------ | ------ | ------ |
| [bmkhw64.dll.2020](../../../raw/master/ExVacuum/bmkhw64.dll.2020) | [#2961](https://dev.vmk.ru/redmine/issues/2961) | Исправлено аварийное завершение работы во время измерения в режиме мультиэкспозиции (Атом 17.02.2021) |
| [ExVacuum2015.dll.2071](../../../raw/master/ExVacuum/ExVacuum2015.dll.2071) | Crash fixed | Rebuilt with VS2015 |
| [ExVacuum2015.dll.2069](../../../raw/master/ExVacuum/ExVacuum2015.dll.2069) | VACUUM_READ_WRITE_COMMAND_BASE | VACUUM_READ_WRITE_COMMAND_BASE |
| [ExVacuum2015.dll.2068](../../../raw/master/ExVacuum/ExVacuum2015.dll.2068) | VACUUM_SLEEP | Recover VACUUM_SLEEP command handling |
| [ExVacuum2015.dll.2061](../../../raw/master/ExVacuum/ExVacuum2015.dll.2061) | [#3242](https://dev.vmk.ru/redmine/issues/3242) | Растягивание последней стадии в соответствии со временем измерения в случае, когда экспозиция в мс - дробная |
| [ExVacuum2015.dll.2019](../../../raw/master/ExVacuum/ExVacuum2015.dll.2019) | [#2887](https://dev.vmk.ru/redmine/issues/2887) | Сборка версии 1974 в Visual Studio 2015 |
| [ExVacuum2010.dll.1974](../../../raw/master/ExVacuum/ExVacuum2010.dll.1974) | | Исправлена работа плагина в случае если выбрано не составное устройство |
| [bmkhw64.dll.1974](../../../raw/master/ExVacuum/bmkhw64.dll.1974) | | Исправлена работа плагина в случае если выбрано не составное устройство |

# 32-bit
| file | redmine | description |
| ------ | ------ | ------ |
