# FTDI (USB-Masi)
# 64-bit

| file | redmine | description |
| ------ | ------ | ------ |
| [bmkhw64-2015.dll.2096-FTDI](../../../raw/master/FTDI/bmkhw64-2015.dll.2096-FTDI) |  | Добавлена поддержка управления статусным регистром |
| [bmkhw64-2015.dll.2023-2024-FTDI](../../../raw/master/FTDI/bmkhw64-2015.dll.2023-2024-FTDI) |  | Добавлена поддержка управления статусным регистром  (на базе 2023)|
| [bmkhw64-2015.dll.2025-FTDI](../../../raw/master/FTDI/bmkhw64-2015.dll.2025-FTDI) | [#3055](https://dev.vmk.ru/redmine/issues/3055) | Исправлена проблема с темновым в 32-бит |
| [bmkhw64-2015.dll.2023-FTDI](../../../raw/master/FTDI/bmkhw64-2015.dll.2023-FTDI) | [#2959](https://dev.vmk.ru/redmine/issues/2959) | Исправлено падение при отключенном устройстве (Атом 17.02.2021), включена автоконфигурация, требуется изменить перекодировку на устройстве |
| [bmkhw64-2015.dll.2016-FTDI](../../../raw/master/FTDI/bmkhw64-2015.dll.2016-FTDI) | [#2888](https://dev.vmk.ru/redmine/issues/2888) | Установлено Денисом (Май 2021) |
| [bmkhw64-2015.dll.2017-__FTDI_TEST_SIGNAL_DIAGNOSIS](../../../raw/master/FTDI/bmkhw64-2015.dll.2017-__FTDI_TEST_SIGNAL_DIAGNOSIS) | | Расширенная диагностика тестового сигнала в Bmkhware.log |
| [bmkhw64-2015.dll.2018-__FTDI_TEST_SIGNAL_DIAGNOSIS-dump](../../../raw/master/FTDI/bmkhw64-2015.dll.2018-__FTDI_TEST_SIGNAL_DIAGNOSIS-dump) | | Расширенная диагностика тестового сигнала в Bmkhware.log с дампом пакетов |

# 32-bit
| file | redmine | description |
| ------ | ------ | ------ |
| [bmkhw32-2015.dll.2096-FTDI](../../../raw/master/FTDI/bmkhw32-2015.dll.2096-FTDI) |  | Добавлена поддержка управления статусным регистром |
| [bmkhw32-2015.dll.2023-2024-FTDI](../../../raw/master/FTDI/bmkhw32-2015.dll.2023-2024-FTDI) |  | Добавлена поддержка управления статусным регистром (на базе 2023) |
| [bmkhw32-2015.dll.2025-FTDI](../../../raw/master/FTDI/bmkhw32-2015.dll.2025-FTDI) | [#3055](https://dev.vmk.ru/redmine/issues/3055) | Исправлена проблема с темновым в 32-бит |
| [bmkhw32-2015.dll.2023-FTDI](../../../raw/master/FTDI/bmkhw32-2015.dll.2023-FTDI) | [#2959](https://dev.vmk.ru/redmine/issues/2959) | Исправлено падение при отключенном устройстве (Атом 17.02.2021), включена автоконфигурация, требуется изменить перекодировку на устройстве |
| [bmkhw32-2015.dll.2016-FTDI](../../../raw/master/FTDI/bmkhw32-2015.dll.2016-FTDI) | [#2888](https://dev.vmk.ru/redmine/issues/2888) | Денис подтвердил работоспособность (21/04/2021) |
