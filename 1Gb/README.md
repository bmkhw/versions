# 1Gb Maes
# 64-bit

| file | redmine | description |
| ------ | ------ | ------ |
| [bmkhw64.dll.2021](../../../raw/master/ExVacuum/bmkhw64.dll.2021) | [#2965](https://dev.vmk.ru/redmine/issues/2965) | Исправлено чтение >300000 расширением буфера, требуется исправить [#2999](https://dev.vmk.ru/redmine/issues/2999) в Атом для диагностики подобных ситуаций (Atom 17.02.2021) |
| [bmkhw64.dll.1973](../../../raw/master/1Gb/bmkhw64.dll.1973) |  | Используется Сергеем Б. (апрель 2021) |

# 32-bit
| file | redmine | description |
| ------ | ------ | ------ |
