# Создание виртуального сетевого интерфейса
cd powershell
setup-VirtualMaes.bat create
# Запуск эмулятора устройства
VirtualMAES.exe [8x2612, default: 14x2048]
# Удаление виртуального сетевого интерфейса
cd powershell
setup-VirtualMaes.bat remove
# Настройка Атом
ip устройства 10.116.221.2 & 10.116.221.3
ip хоста 10.116.221.1
