@echo off
rem goto once
goto cmd

:cmd
echo Start emulator and stay in cmd when finished to restart it in same console
start cmd /k VirtualMAES.exe
goto end

:once
echo Start emulator and exit from console when finished
start VirtualMAES.exe
goto end

:end