rem powershell "& Set-ExecutionPolicy RemoteSigned -Scope Process; & createloopback.ps1"

rem https://www.javatpoint.com/powershell-run-as-administrator
rem https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/start-process?view=powershell-7.1
rem https://stackoverflow.com/questions/20886243/press-any-key-to-continue
rem powershell start-process powershell -verb runas 
rem powershell start-process powershell -verb runas -Wait
rem powershell start-process powershell -ArgumentList "-ExecutionPolicy","RemoteSigned","-File","removeloopback1.ps1" -verb runas -Wait
rem powershell start-process -ArgumentList '-ExecutionPolicy RemoteSigned -File removeloopback1.ps1' powershell -verb runas -Wait -PassThru
rem powershell start-process -ArgumentList '-ExecutionPolicy RemoteSigned -File %cd%\removeloopback.ps1 -NoExit' powershell -verb runas -Wait -PassThru
powershell -ExecutionPolicy RemoteSigned -File removeloopback.ps1

rem powershell -File createloopback.ps1

