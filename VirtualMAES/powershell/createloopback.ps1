# https://gbe0.com/windows/server/create-loopback-interface-with-powershell
# The name for the loopback adapter interface that will be created.
$loopback_name = 'VirtualMAES-10.116.221.1'

# The name for the servers main network interface. This will be updated to allow weak host send/recieve which is most likely required for the traffic to work for the loopback interface.
$primary_interface = 'Ethernet'

# The IPv4 address that you would like to assign to the loopback interface along with the prefix length (eg. if the IP is routed to the server usually you would set the prefix length to 32).
$loopback_ipv4 = '10.116.221.1'
$loopback_ipv4_dev = '10.116.221.2'
$loopback_ipv4_dev2 = '10.116.221.3'
$loopback_ipv4_dev3 = '10.116.221.16'
$loopback_ipv4_dev4 = '10.116.221.17'
$loopback_ipv4_length = '24'

# The IPv6 address that you would like to assign to the loopback interface along with the prefix length (eg. if the IP is routed to the server usually you would set the prefix length to 128). If you are not adding an IPv6 address do not set these variables.
# $loopback_ipv6 = 'fffa::1'
# $loopback_ipv6_length = '128'
# Write-Host "Hello"
Import-Module -Name LoopbackAdapter
if ( $? ) {
	Write-Output LoopbackAdapter imported successfully
} else {
	Write-Output Installing the LoopbackAdapter module
	Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
	Install-Module -Name LoopbackAdapter -MinimumVersion 1.2.0.0 -Force
}

New-LoopbackAdapter -Name $loopback_name -Force

#$interface_loopback = Get-NetAdapter -Name $loopback_name
#$interface_main = Get-NetAdapter -Name $primary_interface

#Set-NetIPInterface -InterfaceIndex $interface_loopback.ifIndex -InterfaceMetric "254" -WeakHostReceive Enabled -WeakHostSend Enabled -DHCP Disabled
#Set-NetIPInterface -InterfaceIndex $interface_main.ifIndex -WeakHostReceive Enabled -WeakHostSend Enabled
#Set-NetIPAddress -InterfaceIndex $interface_loopback.ifIndex -SkipAsSource $True

#Get-NetAdapter $loopback_name | Set-DNSClient –RegisterThisConnectionsAddress $False

# Set the IPv4 address
New-NetIPAddress -InterfaceAlias $loopback_name -IPAddress $loopback_ipv4 -PrefixLength $loopback_ipv4_length -AddressFamily ipv4
New-NetIPAddress -InterfaceAlias $loopback_name -IPAddress $loopback_ipv4_dev -PrefixLength $loopback_ipv4_length -AddressFamily ipv4
New-NetIPAddress -InterfaceAlias $loopback_name -IPAddress $loopback_ipv4_dev2 -PrefixLength $loopback_ipv4_length -AddressFamily ipv4
New-NetIPAddress -InterfaceAlias $loopback_name -IPAddress $loopback_ipv4_dev3 -PrefixLength $loopback_ipv4_length -AddressFamily ipv4
New-NetIPAddress -InterfaceAlias $loopback_name -IPAddress $loopback_ipv4_dev4 -PrefixLength $loopback_ipv4_length -AddressFamily ipv4

# Set the IPv6 address - Uncomment this if required
# New-NetIPAddress -InterfaceAlias $loopback_name -IPAddress $loopback_ipv6 -PrefixLength $loopback_ipv6_length -AddressFamily ipv6

Disable-NetAdapterBinding -Name $loopback_name -ComponentID ms_msclient
Disable-NetAdapterBinding -Name $loopback_name -ComponentID ms_pacer
Disable-NetAdapterBinding -Name $loopback_name -ComponentID ms_server
Disable-NetAdapterBinding -Name $loopback_name -ComponentID ms_lltdio
Disable-NetAdapterBinding -Name $loopback_name -ComponentID ms_rspndr
