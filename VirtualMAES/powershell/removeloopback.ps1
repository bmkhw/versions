Function pause ($message)
{
    # Check if running Powershell ISE
    if ($psISE)
    {
        Add-Type -AssemblyName System.Windows.Forms
        [System.Windows.Forms.MessageBox]::Show("$message")
    }
    else
    {
        Write-Host "$message" -ForegroundColor Yellow
        $x = $host.ui.RawUI.ReadKey("NoEcho,IncludeKeyDown")
    }
}

# https://gbe0.com/windows/server/create-loopback-interface-with-powershell
# The name for the loopback adapter interface that will be created.
$loopback_name = 'BMK-Test-1-Loopback'
$loopback_name = 'VirtualMAES-10.116.221.1'

# The name for the servers main network interface. This will be updated to allow weak host send/recieve which is most likely required for the traffic to work for the loopback interface.
$primary_interface = 'Ethernet'

# The IPv4 address that you would like to assign to the loopback interface along with the prefix length (eg. if the IP is routed to the server usually you would set the prefix length to 32).
$loopback_ipv4 = '10.116.221.1'
$loopback_ipv4_length = '32'

# The IPv6 address that you would like to assign to the loopback interface along with the prefix length (eg. if the IP is routed to the server usually you would set the prefix length to 128). If you are not adding an IPv6 address do not set these variables.
# $loopback_ipv6 = 'fffa::1'
# $loopback_ipv6_length = '128'
# Write-Host "Hello"
try {
	Import-Module -Name LoopbackAdapter
} catch {
	Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
	Install-Module -Name LoopbackAdapter -MinimumVersion 1.2.0.0 -Force
}

try {
	Remove-LoopbackAdapter -Name $loopback_name -Force
} finally {
#	pause ("press any key")
}
