@echo off
rem goto command_%1
rem echo CMDument: %1
SET CMD=%~1
IF [%CMD%] == [] SET CMD=help
IF %CMD% == create GOTO create
IF %CMD% == remove GOTO remove
IF %CMD% == cmdline GOTO cmdline
IF %CMD% == help GOTO help

GOTO done

:create
  ECHO "Creating VirtualMAES-10.116.221.1 adapter, please check via Connections.lnk"
  powershell start-process -ArgumentList '-ExecutionPolicy Unrestricted -F %cd%\createloopback.ps1' powershell -verb runas -Wait
GOTO done


:remove
  ECHO "Removing VirtualMAES-10.116.221.1 adapter, please check via Connections.lnk"
  powershell start-process -ArgumentList '-ExecutionPolicy Unrestricted -F %cd%\removeloopback.ps1' powershell -verb runas -Wait
GOTO done

:cmdline
  ECHO "Starting powershell with admin rights, commands: create<tab> remove<tab>"
  powershell start-process -ArgumentList '-ExecutionPolicy Unrestricted -NoExit cd %cd%; ECHO createloopback.bat removeloopback.bat exit;' powershell -verb runas -Wait
GOTO done

:help
ECHO "Usage: powershell-prompt.bat [create|remove|cmdline|help (default)]"
GOTO done

:done
